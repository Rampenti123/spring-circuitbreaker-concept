package com.circuitbreaker.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.circuitbreaker.model.Order;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@RequestMapping("hello")
public class CatelogController {

	public List<Order> getOrder() {
		List<Order> list = Arrays.asList(new Order(1, "Mobile", "ELectronic", "black", 22000),
				new Order(2, "Laptop", "ELectronic", "black", 50000), new Order(3, "Bottle", "FMCG", "black", 77),
				new Order(4, "Bag", "FMCG", "black", 819), new Order(5, "Oil", "FMCG", "Green", 160),
				new Order(6, "Milk", "FMCG", "White", 35));
		return list;
	}

	private List<Order> getOrderByCategory(String category) {

		List<Order> allOrders = getOrder();
		return allOrders.stream().filter(s -> s.getCategory().equals(category)).collect(Collectors.toList());
	}

	@GetMapping("/all")
	public List<Order> getAllOrder() {
		return getOrder();
	}

	@GetMapping("/getById/{category}")
	public List<Order> getAllOrder(@PathVariable String category) {
		return getOrderByCategory(category);
	}

	

}
