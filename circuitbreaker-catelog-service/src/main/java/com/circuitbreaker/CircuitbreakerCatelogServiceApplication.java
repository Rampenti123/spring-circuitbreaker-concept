package com.circuitbreaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CircuitbreakerCatelogServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CircuitbreakerCatelogServiceApplication.class, args);
	}

}
