package com.circuitbreaker.model;


//@Entity
public class Order {
//	@Id
	private int id;
	private String name;
	private String category;
	private String color;
	private int price;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", name=" + name + ", category=" + category + ", color=" + color + ", price=" + price
				+ "]";
	}

	public Order(int id, String name, String category, String color, int price) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
		this.color = color;
		this.price = price;
	}
	

}
