package com.circuitbreaker.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.circuitbreaker.model.Order;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
public class UserController {

	public static final String USER_SERVICE="userService";

	@Autowired
	RestTemplate restTemplate;
	@GetMapping("/findAllOrder/{category}")
	@CircuitBreaker(name = USER_SERVICE,fallbackMethod = "giveFallBackMessage")
	public List<Order> getAllOrderInCustomer(String category) {
		String url="http://localhost:8081/hello";
	    String urlAfter = category == null ? url+"/all" : url + "/getById/" + category;
        return restTemplate.getForObject(urlAfter, ArrayList.class);		
	}
	public List<Order> giveFallBackMessage(Exception e) {
		List<Order> list = Arrays.asList(new Order(1, "Mobile", "ELectronic", "black", 22000));
		return list;
	}
}
